n, k = map(int, input().split())
nums = [i for i in range(1, n + 1) if i % 2 != 0]
nums.extend([i for i in range(1, n + 1) if i % 2 == 0])
print(nums[k - 1])