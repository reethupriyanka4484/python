def watermelon(n):
    ct, count = 1, 0
    while(ct < n):
        if ct % 2 == 0 and ((n - ct) % 2) == 0:
            count += 1
        ct += 1
    if count >= 1 :
        return "YES"
    else :
        return "NO"
print(watermelon(int(input())))